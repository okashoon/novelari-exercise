class Tweet

    def initialize(tweet)
        @tweet = tweet
    end

    def posted
        @posted = :posted
    end

    def draft
        @dratf = :draft
    end

    def deleted
        @deleted = :deleted
    end
    
end

t = Tweet.new("my tweet")
t.posted

class Tweet2
    attr_accessor :status
    status_array = [:posted, :deleted, :draft]
    status_array.each do |stat|
        define_method stat do
            @status = stat
        end
    end

    def method_missing(method_name, *args)
        puts "missing method #{method_name}"
        @status.send(method_name, args)
    end


end


a = Document.new({author:'ahmed',title:'myDocument',content:'content'})
b = Document.new({author:'mohamed',title:'document2',content:'content2'})
