# Mixing in modules
#[6] Create a method that returns today's date. Method name should be 'current_date'
# This method should be in a module called 'Generic'.
# Try mixing in this module into the Document class
# Now use this method to create another method that returns
# The tite of the document and today's date like below:
# "document_title 2014-06-22". This method should be called 'title_with_date'


#[7] we would like to add a function that replaces a word with another
# replace_word(old_word, new_word)

#Metaprogramming
#[8] instead of calling document.replace_word(old,new) we would like to be able to
# do something like this: document.replace_book('pen') where book is the old word
# and pen is the new word. This should work on any word (not just book).
# hint:  'method_missing'

require 'time'

module Generic
   def Generic.current_date 
        return Time.now.to_s
   end
end


class Document
      def initialize attr
            @author = attr[:author]
            @title = attr[:title]
            @content = attr[:content]

      end

      attr_accessor :author
      attr_accessor :title
      attr_accessor :content

      def +(doc)
            return self.new(self.author, self.title, doc.content)
      end
      
      def +(str)
            @content = @content + str
      end

      def words 
          return @content.split(" ")
      end

      def each_word
          self.words.each{|i| yield i}
      end

      
      def title_with_date
        return @title + " " + Generic.current_date
      end

      def replace_word (old, new)
        new_content = self.words.map do |word| 
            
            if word == old 
                new
            else
                old
            end

        end
        @content = new_content
        
      end


      def method_missing(method_name, *args)
          
        word_to_be_replaced = method_name.slice(8,method_name.length)
        
        new_content = self.words.map do |word| 
            
            if word == word_to_be_replaced 
                puts word
                args[0]
            else
                word
            end
        

        end
        @content = new_content
      end


end

a = Document.new({author:'ahmed',title:'myDocument',content:'content of document 1'})
b = Document.new({author:'mohamed',title:'document2',content:'content2'})